import time
import os
import sys, getopt
import argparse
import string
import fileinput

#Below variables are used for the node pool creation process
CLUSTERNAME="itcluster-sp"
NODEPOOLNAMETOCREATE="itnodepool02"
REGION="us-central1"  #Recommend to use the same region where your existing cluster
DISKSIZE="10"
MACHINETYPE="n1-standard-1"

print("CREATING THE NODEPOOL.......")
#Create nodepool
createpool=os.system("gcloud container node-pools create "+NODEPOOLNAMETOCREATE +" --cluster "+CLUSTERNAME +" --disk-size="+DISKSIZE +" --machine-type="+MACHINETYPE +" --region="+REGION +" --enable-autorepair --enable-autoupgrade")
time.sleep(10)
print("\n")

print("LISTING THE NODEPOOLS INSIDE THE CLUSTER AFTER CREATION.......")
#Deletion of node pool procress starts. Listing all the nodepools inside the cluster
listofpools=os.system("gcloud container node-pools list --cluster="+CLUSTERNAME +" --region="+REGION +"")
print("\n")

print("LET'S DELETE THE NODEPOOL.......")
time.sleep(2)
print("SELECT THE NODEPOOL YOU WANT TO DELETE FROM THE ABOVE LIST OF NODEPOOLS.......")
print("\n")
NODEPOOLNAMETODELETE = raw_input("NODEPOOLNAMETODELETE")
print("DELETING THE NODEPOOL    : " + NODEPOOLNAMETODELETE)

#Delete the noodpool
deletenodepool = os.system("gcloud container node-pools delete "+NODEPOOLNAMETODELETE +" --cluster="+CLUSTERNAME +" --region="+REGION +" --quiet")
time.sleep(1)
print("\n")

print("LISTING THE NODEPOOLS INSIDE THE CLUSTER AFTER DELETION.......")
#Lists all the nodepools inside the cluster
listofpools=os.system("gcloud container node-pools list --cluster="+CLUSTERNAME +" --region="+REGION +"")